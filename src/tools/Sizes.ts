import { Dimensions } from 'react-native';
const HEIGHT = Dimensions.get('window').height
const WIDTH = Dimensions.get('window').width
export default {
    HEIGHT,
    WIDTH,
    header: {
        xl:HEIGHT/2.5,
        lg: HEIGHT / 2.8,
        sm: HEIGHT / 3.8,
        xs: HEIGHT /7.5,
        bodySize: 50,
        iconBox: 40,
        icon: 24,
        marginBetweenIcon: 18

    },
    button:{
        height:40
    },
    top: 18,
    bottom: 18,
    left: 16,
    right: 16

};
