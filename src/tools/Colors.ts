/* In this file all the color codes has been defined with an id */
export const Colors = {
  madarsho_a: '#ff5895',
  madarsho_b: '#91e1dc',
  madarsho__h: '#700e70',
  madarsho_yh: '#c00e70',
  icon_color: '#B3B4C2',
  madarsho_c: '#7f7f7f',
  madarsho_d: '#222222',
  madarsho_f: '#afafaf',
  madarsho_menu: '#b2dfdb',
  madarsho_light: '#e0f2f1',
  madarsho_mid: '#80cbc4',
  madarsho_p: '#6200ee',
  madarsho_g: '#c94f7c',
  madarsho_m: '#a1b7b4',
  madarsho_n: '#c94f7c',
  madarsho: '#ffb2dd',
  healthback: '#4D4C60',

  madarsho_1: '#5BB0BA',
  madarsho_2: '#C15B78',
  madarsho_3: '#EBF5F7',
  madarsho_4: '#FFC3D5',


  madarsho_5: '#FADCDC',
  madarsho_6: '#4D4C60',  //not pragnent
  madarsho_7: '#D691A0',//preper to  pragnent
  madarsho_8: '#C45F90',//pragnent


  madarsho_9: '#daf8e3',
  madarsho_10: '#97ebdb',  //not pragnent
  madarsho_11: '#00c2c7',//preper to  pragnent
  madarsho_12: '#00a6cd',//pragnent
  madasrsho_13: '#005582',

  color_a: '#0D62AA',
  color_b: '#e8f8f9',
  color_b_old: '#27BCC8',
  color_c: '#d2d3d8',
  color_d: '#434656',
  color_e: '#bbd6ed',
  color_f: '#7ec0f7',
  color_m: '#efefef',
  background_color: '#FFFFFF',
  text_color: 'black',
  color_h: '#DCE1E6',
  light_gray_background: '#f3f2f2',
  placeholderColor: '#adadad',
  gray_placeholder: '#7a7a7a',
  tab_deactive: '#FFB9D0',
  topbar: '#3a3a3a',
  a_madarsho: '#F2F3F8',
  b_madarsho: '#04C3A4',
  c_madarsho: '#E74C8B',
  d_madarsho: '#99DCD8',

  d2_madarsho: '#EFFBFA',
  e_madarsho: '#FF7296',
  f_madarsho: '#B3B4C2',
  g_madrsho: '#FBE3ED',
  m_madarsho: '#63D1C0',
  n_madarsho: '#04C3A9',
  j_madarsho: '#EDEFF1',
  k_madarsho: '#738AE8',
  q_madarsho: '#949EBD',
  r_madarsho: '#bbbbbb',
  s_madarsho: '#687293',
  t_madarsho: '#FE6A98',
  gray_madarsho: '#D4D8E5',
  border_madarsho: '#EDEFF1',
  shadow_madarsho: '#707070',
  black_madarsho: '#404040',
  gradiant_a: '#F650A0',
  gradiant_b: '#FB749B',
  gradiant_c: '#FF9897',
  gradiant_d: '#7BE8D7',
  bg_color: '#B4ECE9',
  grayb: '#ADB6BE',

  gray_serach: '#A8B2D0'

};



export const themColor = {
  primary: {
    50: '#ecfeff',
    100: '#cffafe',
    200: '#F650A0',
    300: '#67e8f9',
    400: '#91E1DC',
    500: '#06b6d4',
    600: '#74B4B0',
    700: '#0e7490',
    800: '#155e75',
    900: '#164e63',
  },
  secondary: {
    50: '#ecfeff',
    100: '#F650A0',
    200: '#FB749B',
    300: '#FB749B',
    400: '#FF9897',
    500: '#7BE8D7',
    600: '#0891b2',
    700: '#0e7490',
    800: '#155e75',
    900: '#E06692',
  },
  Success: '',
  Danger: '',
  Warning: '',
  Info: '',
  light: '#F2F3F8',
  Dark: '',
  white: '#FFFFFF',
  Black: ''

}

export const textColor = {
  primary: '#444444',
  secondary: '#949EBD',
  Success: '',
  Danger: '',
  Warning: '',
  Info: '#738AE8',
  Light: '',
  black: '',
  white:'#fff'

}


