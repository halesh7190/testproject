import {StyleSheet, Dimensions, PixelRatio} from 'react-native';
import {textFontSize as typography } from './typography' // normal size for text 

const constStyles = StyleSheet.create({

  /***************** text font style ************************ */
  xxs_text:{
    fontSize: typography.xxs,
  },

  xxs_textBold:{
    fontSize: typography.xxs,
  },

  xs_text:{
    fontSize: typography.xs,
  },

  xs_textBold:{
    fontSize: typography.xs,
  },
  sm_text:{
    fontSize: typography.sm,
  },

  sm_textBold:{
    fontSize: typography.sm,
  },
  md_text:{
    fontSize: typography.md,
  },
  md_textBold:{
    fontSize: typography.md,
  },
  lg_text:{
    fontSize: typography.lg,
  },
  lg_textBold:{
    fontSize: typography.lg,
  },
  xl_text:{
    fontSize: typography.xl,
  },
  xl_textBold:{
    fontSize: typography.xl,
  },


});

export default constStyles;
