import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

/* -------------------------------------------------------------------------- */
/*                          set component for wrapped                          */
/* -------------------------------------------------------------------------- */

export const container = (WrappedComponent) => {
  const hocComponent = ({ ...props }) => {
    return <View style={{ flex: 1 }} />;
  };
  return hocComponent;
};

export default (WrapperComponent) =>
  connect(
    null,
    null
  )(container(WrapperComponent));