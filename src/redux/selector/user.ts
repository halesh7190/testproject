import { createSelector } from 'reselect';

const getUser = (state) => state.userReducer.users
export const userSelector = createSelector(
  getUser,
  users => users
)