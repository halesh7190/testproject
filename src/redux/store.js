import {createStore, applyMiddleware} from 'redux';
import {persistStore} from 'redux-persist';
import thunk from 'redux-thunk';
import AppReducer from './reducer';

const middlewares = [thunk];

// if (__DEV__) {
//   const createDebugger = require('redux-flipper').default;
//   middlewares.push(createDebugger());
// }
export const store = createStore(AppReducer, applyMiddleware(...middlewares));

export let persistor = persistStore(store);

export default store;
