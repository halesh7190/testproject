import { useSelector, useDispatch } from 'react-redux'
import {userSelector} from '../selector/user';



export const useRedux = () => {
const dispatch = useDispatch()
const users = useSelector(userSelector);

    return {
        users,
       dispatch
    
    }
}
