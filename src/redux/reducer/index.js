import {persistCombineReducers} from 'redux-persist';
// import storage from 'redux-persist/lib/storage';
import {AsyncStorage} from 'react-native';
import userReducer from './userReducer';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['userReducer'],
};

const AppReducer = persistCombineReducers(persistConfig, {
  userReducer,
});

export default AppReducer;
