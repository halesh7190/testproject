import {IUser} from '../../logic/DTO';
import {GET_USERS} from '../actionType';

let defaultUsers:IUser[]=[]
const INITIAL_STATE = {
    users:defaultUsers
  };
const  userReducer=(state = INITIAL_STATE, action)=> {

  switch (action.type) {
    case GET_USERS: {
    return {...state,users:action.payload};
    }
    default:
      return state;
  }
}
export default userReducer;