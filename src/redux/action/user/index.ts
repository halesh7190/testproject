
import {IUser} from '../../../logic/DTO'
import {GET_USERS
} from '../../actionType'

export const setUsersAction = (users:IUser[]) => {

    return {
        type: GET_USERS,
        payload: users
    }
  };
