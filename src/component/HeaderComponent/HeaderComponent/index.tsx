
import React from 'react';
import { View, Text, Image, StatusBar, ImageBackground, FlatList, TouchableOpacity } from 'react-native';
import styles from './styles';
import { Header, Button } from 'native-base'
import Sizes from '../../../tools/Sizes';
import BackIcon from 'react-native-vector-icons/AntDesign'
import { useState } from 'react';
import { textColor } from '../../../tools/Colors';



export interface Props {
    backgroundColor: string,
    navigation: any, //TODO :most have type
    statusBarColor?: string,
    height?: number,
    headerTitle: string,
    mainHeader?: string,
    showSearch?: boolean,
    showBackIcon?: boolean,
    showAvatar?: boolean

}
const HeaderComponent: React.FC<Props> = props => {

    const [focusSearch, setFocusSearch] = useState(false)
    const [searchModalVisible, setSearchModalVisible] = useState(false)
    const [SearchText, setSearchText] = useState('');

    const heightWhitSearch = Sizes.HEIGHT / 2.5;
    const height = searchModalVisible ? 300 : props.height;

  
    const back = () => {
        props.navigation.goBack()
    };





    return (
        <Header style={[styles.header, { backgroundColor: props.backgroundColor, height: props.height, }]}>
            <StatusBar
                backgroundColor={props.statusBarColor}
                barStyle={'light-content'}
            />

            <View style={[styles.wrapper, { flexDirection: 'column', justifyContent: 'flex-start', height }]}>

                <View style={styles.wrapperIntoWrapper}>

                    <View style={[styles.left, { height: searchModalVisible ? heightWhitSearch : Sizes.header.bodySize, zIndex: 20 }]}>
                        <Button
                            style={[styles.button, { alignSelf: 'flex-start' }]}
                            transparent
                            onPress={() => props.showBackIcon ? back() : props.navigation.toggleDrawer()}
                        >
                           {/* <BackIcon name={'arrowright'} size={20} color={textColor.primary}/>  */}

                        </Button>
                    </View>
                        <View style={[styles.body]}>

                        <Text style={[styles.title]}>{props.headerTitle}</Text>

                        </View>

                    <View style={styles.right}>
                        {(props.showAvatar === undefined || props.showAvatar) &&

                            <Button
                                style={styles.button}
                                transparent
                                onPress={() => back()}
                            >
                            </Button>
                        }
                    </View>
                </View>


            </View>

        </Header>
    );
};


export default HeaderComponent


