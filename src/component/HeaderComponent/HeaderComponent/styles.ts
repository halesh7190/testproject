import { StyleSheet } from 'react-native';
import { textColor, themColor } from '../../../tools/Colors';
import constStyles from '../../../tools/constStyle';
import Sizes from '../../../tools/Sizes';
export default StyleSheet.create({
    header: {
        height: Sizes.header.lg,
        width: Sizes.WIDTH,
        elevation: 0,
    

    },
    wrapper: {
        flexDirection:'row-reverse',
        height: Sizes.header.lg,
        width: Sizes.WIDTH ,
        paddingLeft: Sizes.left,
        paddingRight: Sizes.right,
        justifyContent: 'center',

    },
    wrapperIntoWrapper:{
        height:Sizes.header.bodySize,width:'100%',flexDirection:'row-reverse'
        ,marginTop:16,
        justifyContent:'flex-start',
        alignItems:'flex-start'
    },
    left: {
        height: Sizes.header.bodySize,
        flexDirection: 'row-reverse',

    },
    body: {
        flex: 3,
        // width:(((Sizes.WIDTH)-3*Sizes.header.iconBox)-Sizes.left-Sizes.right),
        height: Sizes.header.bodySize,
        justifyContent: 'center',
        alignItems:'center',
    },
    right: {
        height: Sizes.header.bodySize,
        flexDirection: 'row-reverse',
        justifyContent: 'flex-end',
    },
    button: {
        width: Sizes.header.iconBox,
        height: Sizes.header.iconBox,
        justifyContent: 'center',
        alignItems: 'center',
    
    },
    box:{
     borderRadius:0,
     borderBottomLeftRadius:25,
    borderBottomRightRadius:25,
    backgroundColor:'white',
    width:(((Sizes.WIDTH)-2*Sizes.header.iconBox)-Sizes.left-Sizes.right)-10,
    padding:5,
     },

    rowBox:{width:'100%',height:40,flexDirection:'row-reverse',marginBottom:1,alignItems:'center'}, 
    icon: {
        height: Sizes.header.icon,
        width: Sizes.header.icon,
    },
    title:{
     ...constStyles.sm_textBold,
     color:textColor.white
    },
     text:{
     ...constStyles.sm_text,
     color:textColor.secondary   
    },

    mainBody:{
      flexDirection:'column',justifyContent:'center',
      width:'100%',alignItems:'center'
    },
    mainWrapper:{
    width:'100%',justifyContent:'center',alignItems:'center'
    },
    mainCenter:{
        width:'100%',flexDirection:'row-reverse',justifyContent:'center',alignItems:'center',
    },
    mainButton:{
    width:60,height:'100%',justifyContent:'flex-end',alignItems:'center'
    },
    weekWrapper:{
    width:'100%',height:42,
    // justifyContent:'center',
    alignItems:'center',
    flexDirection:'row-reverse',
 
    // backgroundColor:'rgba(0,0,0,0.5)',
    marginTop:10
    },
    weekNumber:{
     ...constStyles.lg_text,
     color:textColor.white   ,
  
    },
    weekNumberCenter:{
     ...constStyles.lg_textBold,
     color:'#E74C8B',
    },
    circleWeekNumber:{
height:32,width:32,borderRadius:16,backgroundColor:themColor.white,justifyContent:'center',alignItems:'center'
    },
    smallText:{
     ...constStyles.xxs_text,
     color:textColor.white  ,
   
    },
});
