import { StyleSheet } from 'react-native';
import { themColor, textColor } from '../../tools/Colors';
import constStyles from '../../tools/constStyle';
export default StyleSheet.create({
    container: {

        flex: 1,
        alignItems: 'center',
        backgroundColor: themColor.light,
        justifyContent: 'center',

    },
    Image: {
        height:60,
        width: 60,
        tintColor: themColor.secondary[400]
    },
    title: {
        marginTop:10,
        color: textColor.primary,

    }
});
