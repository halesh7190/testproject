
import React from 'react';
import { View, Text, Image } from 'react-native';
import styles from './styles';
// import { WI_FI_ICON } from '../../tools/Images'

const NetErrorComponent: React.FC = () => {
    return (
        <View
            style={styles.container}>
            {/* <Image
                source={WI_FI_ICON}
                style={styles.Image}
            /> */}
            <Text
                style={styles.title}>
                لطفا اینترنت خود را روشن کنید
            </Text>
        </View>
    );
};

export default NetErrorComponent
