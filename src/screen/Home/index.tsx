/* ------------------------------------------------------------------------- */
/*                          startUserInfo component                          */
/* ------------------------------------------------------------------------ */
/************************  react component ****************************** */
import React, { useState,useRef } from 'react';
import { Text, View, SafeAreaView, FlatList ,Animated,Image} from 'react-native';
/************************ dependency component ************************** */

/************************    other thing      ************************** */
import { Colors, themColor } from '../../tools/Colors';
import styles from './styles';
import HomeLogic from './HomeLogic'
import { avatar } from '../../tools/Images';
import { IUser } from '../../logic/DTO';

export interface Props {
    navigation: any; //TODO
}

const StartUserInfo: React.FC<Props> = (props) => {


    const {users } = HomeLogic(props.navigation)
    const scrollY=useRef(new Animated.Value(0)).current;
    const heightBox=100;
     const imageBox=50;
    const spacing=20;
    const ItemSize=heightBox +spacing *3;

    const renderItem=(item:IUser,index)=>{
                const inputRange=[
                    -1,0,ItemSize*index,ItemSize*(index +1)
                ]
                const scale=scrollY.interpolate({
                    inputRange,
                    outputRange:[1,1,1,0]
                })
                const opacityInputRange=[
                    -1,0,ItemSize*index,ItemSize*(index +1)
                ]
                const opacity=scrollY.interpolate({
                    inputRange:opacityInputRange,
                    outputRange:[1,1,1,0]
                })
                return(
                    <Animated.View style={{
                    ...styles.wrapperBox,
                    transform:[{scale:scale}],
                    opacity,
                    height:heightBox,
                    marginBottom:spacing
                    }}
                     >
                        <Image source={avatar} style={{width:imageBox,height:imageBox}}/>
                        <View style={{marginLeft:20}}>

                        <Text style={{fontSize:16,fontWeight:'700'}}>{item.name}</Text>
                        <View style={{flexDirection:'row'}}>
                        <Text style={{fontSize:14,opacity:0.8,marginRight:5}}>phone number:</Text>
                        <Text style={{fontSize:12,opacity:0.8}}>{item.phone}</Text>
                        </View>
                        <Text style={{fontSize:12,color:'#0099cc'}}>{item.email}</Text>
                     
                        </View>
                    </Animated.View>
                )
            }

    

    return (
        <SafeAreaView style={styles.container}>
            <Animated.FlatList
            onScroll={Animated.event(
                [{nativeEvent:{contentOffset:{y:scrollY}}}],
                {useNativeDriver:true}
            )}
            data={users}
            keyExtractor={(item)=>item.id}
            contentContainerStyle={{padding:spacing,paddingTop:spacing+20}}
            renderItem={({item,index})=>renderItem(item,index)}
            
            />
        </SafeAreaView>
    )
}

export default StartUserInfo




