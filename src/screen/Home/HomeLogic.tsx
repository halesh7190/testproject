import React, { useEffect } from 'react';

import { useRedux } from '../../redux/useRedux';
import getUserApi from '../../services/getUserApi'



const logic = (navigation) => {

    const { dispatch, users } = useRedux()

    
    
    useEffect(()=>{
        const api=new getUserApi()
        api.callApi()
    },[])



    return {
        users

    }
}
export default logic