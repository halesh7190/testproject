import { StyleSheet } from 'react-native';
import { themColor, Colors, textColor } from '../../tools/Colors';
import constStyles from '../../tools/constStyle';
import Sizes from '../../tools/Sizes';

export default StyleSheet.create({
    container: {
       flex: 1,backgroundColor:themColor.white 
    },
    wrapperBox:{
        flexDirection:'row',backgroundColor:themColor.light,width:'100%',padding:10,  alignItems:'center',     
                    shadowColor: 'black',
                    borderRadius:10,
                    shadowOpacity: 0.8,
                    shadowOffset: {width: 0, height: 10},
                    shadowRadius: 20,
                    elevation: 3,
    },

    txtStyle: {
        textAlign: 'center',
        color: textColor.primary,
        ...constStyles.sm_textBold,
        marginTop: 10,
        marginBottom: 25
    },


});
