import React from 'react';
import { Image, View, StatusBar,Text } from 'react-native';
import styles from './styles';
import Sizes from '../../tools/Sizes';


const Splash: React.FC = () => {
    const [hidden, setHidden] = React.useState(true);
    return (

        <View style={styles.container}>
            <StatusBar hidden={hidden} />
             <Text style={styles.text}>Splash screen</Text>   
        </View>
    )

};

export default Splash