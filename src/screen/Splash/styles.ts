import { StyleSheet } from 'react-native';
import { textColor, themColor } from '../../tools/Colors';
import constStyles from '../../tools/constStyle';
export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: themColor.primary[900],
        alignItems: 'center',
        justifyContent:'center'
    },
    text:{
        ...constStyles.md_textBold,
        color:textColor.white
    }

});
