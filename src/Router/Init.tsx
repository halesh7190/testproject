/* -------------------------------------------------------------------------- */
/*                                 Init Function                              */
/* -------------------------------------------------------------------------- */
/* 
  At first start app whit this function 
*/

/************************  react component ****************************** */
import React, { useEffect } from 'react';
import { useNetInfo } from '@react-native-community/netinfo';
import { AlertStatic as Alert, Image, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';

/************************ dependency component ****************************** */
import { Splash } from '../screen';
import NetErrorComponent from '../component/NetErrorComponent';
/************************  other things  *********************************** */
import { RootStackScreen } from '.';

// const Router = ({navigation}) => {
const Init = () => {
  const netInfo = useNetInfo();
  const [isLoading, setIsLoading] = React.useState(true);



  useEffect(() => {
    setTimeout(async () => {
      setIsLoading(false);
    }, 1000);

  }, []);




  if (isLoading) {
    return <Splash />;
  }

  return (
    <View style={{ flex: 1 }}>
      <View style={{ flex: 1 }}>
        {netInfo.isConnected === false ? (
          <NetErrorComponent />
        ) : (
          <NavigationContainer  >
            <RootStackScreen />
          </NavigationContainer>
        )}
      </View>
    </View>
  );
};

export default Init;
