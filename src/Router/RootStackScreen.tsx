import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Home } from './../screen';
import { HOME } from './routeName';
import { HeaderComponent } from '.'
import { themColor } from '../tools/Colors';
import Sizes from '../tools/Sizes';
// import { USERS_HEADER } from '../tools/Images';


const RootStack = createStackNavigator();

const RootStackScreen = (navigation) => {


    return (

        <RootStack.Navigator>
            <RootStack.Screen
                options={() => ({
                    header: () => {
                        return (<HeaderComponent
                            backgroundColor={themColor.primary[900]}
                            statusBarColor={themColor.primary[900]}
                            showBackIcon={false}
                            navigation={navigation}
                            headerTitle={'home'}
                            height={Sizes.header.xs
                            }

                        />)
                    }
                })}
                name={HOME}
                component={Home}
            />
        </RootStack.Navigator>

    );
};

export default RootStackScreen
