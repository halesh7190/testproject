/***
 * after call api register if result api= success fill registerData in redux else show error 
 */
import ConnectService ,{METHOD_TYPE} from './ConnectService';
import { IAddress, ICompony, IGeo, IUser} from '../logic/DTO';
import {setUsersAction} from '../redux/action/user';
import {store} from '../redux/store';
// import {  SnackView } from '../global/helper';





interface IUsers{
    users:IUser[]
}

class RegisterApi implements IUsers{


    protected _users=null;

    public async callApi(){
     
        
        const api=new ConnectService('users');
        let result=await api.call()
   
        
        if(result.data!==undefined && result.status===true){
           

            
            this.setAllData(result.data)
            this.getAllData();
            // successCallBack()
        }else{
            //TODO
            //show error message
            result.message.map(ms=>{
            //  SnackView(ms);
            })
            if(result.devMessage!==''){
                //  SnackView('خطایی در سرور اتفاق افتاده');
            }
            // errorCalBack()
        }


       
    }


    public setAllData(result){

        let items:IUser[]=[]
        result.map((item)=>{
            let geo:IGeo={lat:item.address.geo.lat,lang:item.address.geo.lang};
            let address:IAddress={
                 street:item.address.street,
                suite:item.address.suite,
                city:item.address.city,
                zipcode:item.address.zipcode,
                geo
                };
            let company:ICompony={
                name:item.compony?.name!==undefined?item.compony.name:null,
                catchPhrase:item.compony?.catchPhrase!==undefined?item.compony.catchPhrase:null,
                bs:item.compony?.bs!==undefined?item.compony.bs:null,
                }
            let user={
            id:item.id,
            name:item.name,
            username:item.username,
            email:item.email,
            phone:item.phone,
            website:item.website,
            address,
            company,
            }

            items.push(user)
        })

        
        this.users=items
 
    }
    /**
     * fill state managemen
     * @param data 
     */
    public getAllData(){

       store.dispatch(setUsersAction(this.users)) 
    }

    public set users(items:IUser[]){
        this._users=items;

    }

     public get users(){
      return  this._users;

    }



    public isEmpty(items){
        if(items==undefined || items===null){
            return true
        }else{
            return false
        }
    }

}

export default RegisterApi