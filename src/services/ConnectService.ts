
import axios from 'axios';

export enum METHOD_TYPE{
POST="POST",
GET="GET",
DELETE="DELETE"
}
const BASE_URl="https://jsonplaceholder.typicode.com/";
const defaultHeaders = {
   Accept: "application/json",
   'Content-Type': 'application/json'
};
const defaultMethod=METHOD_TYPE.GET;

interface IRequest{
header?:any,
method?:METHOD_TYPE,
data?:any,
uri:string,
}

class ConnectService implements IRequest {

protected _header=defaultHeaders;
protected _method=defaultMethod;
protected _data=null;
protected _uri;

constructor(uri){
  this.uri=uri;
}


public set uri(newUri:string){
 this._uri=newUri

}

public get uri(){
  return this._uri  
}


public set data(newData:any){
 this._data=newData

}

public get data(){
  return this._data  
}


public set method(newMethod:METHOD_TYPE){

 if(newMethod===undefined || newMethod===null) {
    this._method=defaultMethod
 }else{
     this._method=newMethod
 }  

}

public get method(){

    return this._method

}

public set header(newHeader:any){

 if(newHeader!==undefined && newHeader!==null) {
    this._header={...defaultHeaders,...newHeader}
 }
    this._header=newHeader

}

public get header(){
   
return this._header  

}


public async call(){

    try {
        let result=await axios({
            url:`${BASE_URl}${this.uri}`,
            timeout: 1000,
            headers:this.header,
            method:this.method,
            data:this.data

            })
            


            switch (result.status){
                    case 200:
                         return this.handleResponse(true,[''],'',result.data)
                    case 401: {this.Handle401();return this.handleResponse(true,[''],'',null)}

                    case 403:{this.Handle403();return this.handleResponse(false,[''],'',null)}

                    case 500:{this.Handle500();return this.handleResponse(false,[''],'',null)}
                        
                    case 400:{this.Handle400();return this.handleResponse(false,[''],'',null)}

                    case 404:{this.Handle400();return this.handleResponse(false,[''],'',null)}

                   default:{return this.handleResponse(false,[''],'',null)}
             
                        }
                    

    } catch (e) {
        console.log('error',{e});
       return this.handleResponse(false,[''],'',null)

    }
  
}

public handleResponse(status:boolean,message:string[],devMessage:string,response:any){
 
  
    
    let result;
       if(status===true){
      
            result={
                  data:response,
                  status:true
                  }
             
       }else{
           result={
            message:message, // error message for user 
            devMessage:devMessage, // error message for developer 
            status:false 
           }
       } 

    return result    
}

public Handle401(){

}
public Handle403(){
    
}
public Handle500(){
    
}

public Handle400(){

}






}

 export default ConnectService