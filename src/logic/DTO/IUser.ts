import {IAddress,ICompony} from './';


export interface IUser{

    id:number | null,
    name:string | null,
    username:string | null,
    email:string |null,
    phone:string |null,
    website:string |null,
    address:IAddress | null,
    company:ICompony | null,

}