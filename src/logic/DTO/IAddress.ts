import { IGeo } from "./IGeo";


export interface IAddress{
    street:string | null,
    suite:string | null,
    city:string | null,
    zipcode:string | null,
    geo:IGeo | null


}
