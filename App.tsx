import React from "react";
import { View, Text } from "react-native";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { store, persistor } from "./src/redux/store";
import Router from './src/Router';

const App: React.FC = () => {
    return (
        <View style={{ flex: 1, backgroundColor: 'red' }}>
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <Router />
                </PersistGate>
            </Provider>
        </View>
    );
};

export default App;